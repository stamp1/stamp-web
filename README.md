# stamp-web

Backend repository [here](https://gitlab.com/stamp1/stamp-back).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
### Lints and fixes files
```
npm run lint
```

## Docker
```
docker-compose up -d
```
And just open http://localhost/

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
