import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './plugins/axios'
import sound from './plugins/sound'
import webSockets from "@/plugins/web-sockets"
import Toaster from "@meforma/vue-toaster"
import electron from "@/plugins/electron"
import tools from "@/plugins/tools";

const app = createApp(App)
    .use(electron)
    .use(store)
    .use(webSockets)
    .use(router)
    .use(axios)
    .use(sound)
    .use(tools)
    .use(Toaster, {pauseOnHover: true})
    .mount('#app')

export default app
