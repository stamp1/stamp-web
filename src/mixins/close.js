let that;

function onKeyUp (evt) {
    if (evt.code === 'Escape') {
        that.close();
    }
}

export default {
    mounted() {
        that = this;

        document.addEventListener('keyup', onKeyUp);
    },
    unmounted() {
        document.removeEventListener('keyup', onKeyUp);
    }
};