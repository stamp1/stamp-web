import { createStore } from 'vuex'
import auth from "@/store/auth";
import chats from "@/store/chats/index";
import users from "@/store/users";

export default createStore({
  modules: {auth, chats, users},
})
