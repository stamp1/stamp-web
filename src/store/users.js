import UserAPI from '@/api/user.api'
export default {
    namespaced: true,
    state: () => ({
        currentUser: '',
        users: [],
    }),
    mutations: {
        addUser: (state, user) => { state.users.push(user) }
    },
    actions: {
        async loadCurrentUser({state}) {
            const user = await UserAPI.getCurrentUser()
            user.accountOwner = true
            state.users.push(user)
            state.currentUser = user._id
        },
        async loadUsers({state}) {
            const users = await UserAPI.getFriends();
            state.users.push(...users);
        },
        addUserClean({state, commit}, newUser) {
            const isset = ~state.users.findIndex( user => user._id === newUser._id )
            if (!isset) commit('addUser', newUser)
        },
        removeData({state}) {
            state.currentUser = '';
            state.users = [];
        },
    },
    getters: {
        userById: store => id => store.users.find(user => user._id === id),
        currentUser: (store, getters) => getters['userById'](store.currentUser),
        getUserName: (store, getters) => id => {
            const user = getters['userById'](id)
            if (!user) return '???'
            return user?.accountOwner ? 'Me' : user.login
        },
    }
}
