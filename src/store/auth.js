import AuthAPI from '@/api/auth.api'
import router from "@/router";
import app from "@/main";

export default {
    namespaced: true,

    state: () => ({
        token: localStorage.getItem('stamp-vue-token') || '',
        inited: false,
    }),
    mutations: {
        auth: (state, token) => {
            state.token = token;
            localStorage.setItem('stamp-vue-token', token);
        },
        logout: (state) => {
            state.token = '';
            state.inited = false
            localStorage.removeItem('stamp-vue-token');
        }
    },
    actions: {
        signIn: async ({commit}, payload) => {
            let res;
            try {
                res = await AuthAPI.login(payload.login, payload.password);
            } catch (e) {
                res = e?.response;
                console.error(e?.response?.data?.message || e);
            }
            if (res?.status === 404) {
                const reg = await AuthAPI.register(payload.login, payload.password);
                res = await AuthAPI.login(payload.login, payload.password);
                app.$toast.info(reg.data.message);
            }

            if (res?.status === 200) {
                commit('auth', res?.data.token || '');
                await router.push('/chat')
            }
        },
        async logout({commit, dispatch}) {
            commit('logout')
            dispatch('users/removeData', null, { root: true })
            await router.push('/')
            await AuthAPI.logout()
        },
        async loadData({commit}) {
            const data = await AuthAPI.getData();
            commit('loadData', data);
        },
        async initialize({dispatch, state}) {
            // load necessary data
            await dispatch('users/loadCurrentUser', null, { root: true })
            await dispatch('users/loadUsers', null, { root: true })
            await dispatch('chats/loadChats', null, { root: true })
            app.$ws.init()
            state.inited = true;
        }
    },
    getters: {
        inited: state => state.inited,
        authed: state => !!state.token,
        loaded: state => !!state.data,
    }
}
