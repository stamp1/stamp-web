import ChatsAPI from "@/api/chats.api";
import app from "@/main";
import {$electron, Electron} from "@/plugins/electron";
import tools from "@/plugins/tools";

export default {
    setEdited: ({commit, getters}, id) => {
        const message = getters.messageById(id)
        commit('setEdited', id)
        commit('setReplied', '')
        commit('setInputText', message.content)
        commit('setFocusTextInput')
    },
    setReplied: ({commit}, messageId) => {
        commit('setReplied', messageId)
        commit('setEdited', '')
        // commit('setInputText', '')
        commit('setFocusTextInput')
    },
    setLastEdited: ({getters, dispatch}) => {
        const lastMsgIndex = getters['currentChat'].messages.length - 1;
        const lastMsg = getters['currentChat'].messages[lastMsgIndex];
        dispatch('setEdited', lastMsg._id);
    },
    setCurrentChat: ({commit, dispatch}, id) => {
        dispatch('resetTempState')
        commit('setCurrentChat', id)
    },
    resetTempState({commit}) {
        commit('setInputText', '')
        commit('setReplied', '')
        commit('setEdited', '')
    },
    async loadChats({state}) {
        let chats = (await ChatsAPI.loadChats()).data;
        chats = Object.values(chats);
        state.chats = chats.length > 0 ? chats : [];
    },
    async loadChat({commit, dispatch}, chatId) {
        try {
            const chatCandidate = await ChatsAPI.getChat(chatId);
            for (let key in chatCandidate.interlocutors) {
                const user = chatCandidate.interlocutors[key];
                dispatch('users/addUserClean', user, {root: true})
                chatCandidate.interlocutors[key] = chatCandidate.interlocutors[key]._id
            }
            commit('addChat', chatCandidate)
        } catch (e) {
            console.error(e);
        }
    },
    async selectChat({dispatch, getters}, chatId) {
        // find in store
        let chatCandidate = getters.chatById(chatId);

        if (!chatCandidate) {
            // find in api
            dispatch('loadChat', chatId)
            chatCandidate = getters['chatById'](chatId);
        }

        if (chatCandidate) {
            dispatch('setCurrentChat', chatCandidate._id)
        } else {
            console.error(`Chat with id ${chatId} not found! Some error...`)
            app.$toast.error('chat not found')
        }
    },
    async selectUser({getters, commit, dispatch}, user) {
        let dialog = getters.dialogByUserId(user._id)
        if (dialog) {
            dispatch('setCurrentChat', dialog._id)
            return;
        }
        dialog = await ChatsAPI.createChat(user._id)

        commit('users/addUser', user, { root: true })
        commit('chats/addChat', dialog, {root: true})
        dispatch('setCurrentChat', dialog._id)
    },
    async sendMessage({state, rootState}, data) {
        data.sender = rootState.users.currentUser;
        const chat = state.chats.find(chat => chat._id === data.chatId);
        const pos = chat.messages.length;
        chat.messages.push(data);
        const res = await ChatsAPI.sendMessage(data);
        chat.messages[pos] = res;
        // put new message data in store
    },
    async saveEdited({getters, commit, state}, val) {
        const message = getters.editedMessage;
        message.content = val;
        message.status = 'loading';
        const updatedMessage = await ChatsAPI.saveEdited(state.edited, val);
        commit('setEdited', '');
        message.status = updatedMessage.status;
    },
    async readMessage({getters}, messageId) {
        const msg = getters.messageById(messageId);
        msg.status = 'read';
        await ChatsAPI.readMessage(messageId);
    },
    async deleteMessage({commit, getters}, messageId) {
        const message = getters.messageById(messageId);
        message.status = 'loading';
        await ChatsAPI.deleteMessage(messageId);
        commit('deleteMessage', messageId);
    },
    goToMessage({getters}, messageId) {
        const candidate = getters.messageById(messageId);
        const chat =  getters.dialogByUserId(candidate.sender);
        console.log(chat);
    },
    async deleteChat({commit}, chatId) {
        await ChatsAPI.deleteChat(chatId)
        commit('deleteChat', chatId)
    },
    addMessage: async ({getters, commit, dispatch}, data) => {
        // from sockets
        const chat = getters.chatById(data.chatId)
        if (!chat) {
            // if we have not chat with this chatId
            const chatCandidate = await ChatsAPI.getChat(data.chatId)
            chatCandidate.interlocutors.forEach( (interlocutor, key) => {
                dispatch('users/addUserClean', interlocutor, { root: true })
                chatCandidate.interlocutors[key] = interlocutor._id;
            })
            commit('addChat', chatCandidate)
        } else {
            // if we have chat with chatId, we will just add message
            chat.messages.push(data.message);
        }
    },
    async newMessageEvent({dispatch, getters}, data) {
        let chatName = getters['chatName'](data.destination)
        if (!chatName) {
            // создать чат
            await dispatch('loadChat', data.destination)
            chatName = getters['chatName'](data.destination)
        } else {
            // просто докинуть сообщение в существующий чат
            dispatch('addMessage', {
                message: data.message,
                chatId: data.destination
            })
        }
        if (!tools.focus || getters['currentChat']?._id !== data.destination) {
            dispatch('sendNotification', {
                title: chatName,
                content: data.message.content,
                callback: () => dispatch('selectChat', data.destination)
            });
        }
    },
    sendNotification(store, {title, content, callback}) {
        app.$sound.newMessage()
        if (Electron.isElectron()) {
            $electron.ipcRenderer.send('new_chat_message', {
                title,
                content
            })
        } else {
            app.$toast.info(`
                <h3>${title}</h3>
                <p>${content}</p>
            `, {
                onClick: () => callback(),
            })
        }
    }
}
