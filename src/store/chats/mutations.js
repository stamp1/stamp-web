export default {
    setInputText: (state, text) => {
        state.inputText = text;
    },
    setCurrentChat: (state, id) => {
        state.currentChat = id;
    },
    addChat: (state, chat) => { state.chats.push(chat) },
    setEdited: (state, messageId) => state.edited = messageId,
    setReplied: (state, messageId) => state.replied = messageId,
    deleteMessage: (state, messageId) => {
        state.chats.every((chat) => chat.messages = chat.messages.filter(message => message._id !== messageId))
    },
    setInputTextRef: (state, ref) => {
        state.inputTextRefs = ref;
    },
    setFocusTextInput(state) {
        state.inputTextRefs.focus();
        // state.inputTextRefs.selectionStart = state.inputTextRefs.value.length;
    },
    deleteChat: (state, chatId) => {
        state.chats = state.chats.filter((chat) => chat._id !== chatId )
    },
    readMessage: (state, data) => {
        const chat = state.chats.find(chat => chat._id === data.chatId)

        const message = chat.messages.find(message => message._id === data.messageId)

        message.status = data.status
    }
}
