export default {
    chatById: (state) => id => state.chats.find(chat => chat._id === id),
    dialogByUserId: (state, getters, rootState) => userId => {
        return state.chats.filter(chat => chat.chatType === 'dialog').find(chat => {
            const currentUserId = rootState.users.currentUser;
            if (~chat.interlocutors.indexOf(currentUserId)
                && ~chat.interlocutors.indexOf(userId)) {
                return true;
            }
        });
    },
    chatList: state => state.chats,
    currentChat: state => state.chats.find(chat => chat._id === state.currentChat),
    currentInterlocutor: (state, getters, rootState, rootGetters) => {
        const currentChat = getters['currentChat']
        const user = rootGetters['users/currentUser']
        if (currentChat.chatType === 'dialog') {
            const interlocutorId =
                currentChat
                    .interlocutors
                    .find(interlocutor => interlocutor !== user._id)
            const interlocutor = rootGetters['users/userById'](interlocutorId)
            return interlocutor
        }
    },
    chatName: (state, getters, rootState, rootGetters) => id => {
        const chat = getters['chatById'](id);
        if (!chat) return false;
        const currentUser = rootGetters['users/currentUser']
        if (chat.chatType === 'dialog') {
            const interlocutorId =
                chat
                    .interlocutors
                    .find(interlocutor => interlocutor !== currentUser._id)
            return rootGetters['users/getUserName'](interlocutorId)
        }
    },
    currentChatName: (state, getters) => getters['chatName'](state.currentChat),
    messageById: (state) => messageId => {
        let message;
        state.chats.every(chat => {
            message = chat.messages.find(message => message._id === messageId)
            return !message;
        })
        return message;
    },
    repliedMessage: (state, getters) => state.replied ? getters.messageById(state.replied) : false,
    editedMessage: (state, getters) => state.edited ? getters.messageById(state.edited) : false,
}
