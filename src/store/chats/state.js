export default () => ({
    inited: false,
    chats: [],
    currentChat: null,
    socket: null,
    inputText: '',
    inputTextRefs: '',
    replied: '',
    edited: '',
})