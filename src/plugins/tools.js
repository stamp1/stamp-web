class Tools {
    constructor() {
        this.interval = 0;
        this.focus = true;
        if (this.interval) clearInterval(this.interval)
        window.addEventListener('focus', () => this.focus = true)
        window.addEventListener('blur', () => this.focus = false)
    }
    install(app) {
        app.config.globalProperties.$tools = this;
    }


}

const tools = new Tools()

export default tools
