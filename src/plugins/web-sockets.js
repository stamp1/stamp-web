import io from "socket.io-client";
import app from "@/main";

class WebSockets {
    install(app) {
        app.config.globalProperties.$ws = this;
    }
    init() {
        this.socket = io.connect(process.env.VUE_APP_API_URL, {
            transports: ["websocket", "polling"],
            query: {
                token: app.$store.state.auth.token.replace('Bearer ', '')
            }
        });
        this.socket.on('connect_error', function(e){
            app.$toast.error(`WS: ${e.message}`)
        });
        this.socket.on('reconnect', function(){
            app.$toast.warning(`WS: Reconnect`)
        });
        this.socket.on('news', function (data) {
            console.log(data);
        });
        this.socket.on('connect', () => {
            this.socket.emit('set_online');
        });
        this.socket.on('new_chat_message', async data =>
            app.$store.dispatch('chats/newMessageEvent', data));
        this.socket.on('changeMessageStatus', data => {
            app.$store.commit('chats/readMessage', {
                messageId: data.message._id,
                chatId: data.destination,
                status: data.status
            })
            console.log('changeMessageStatus', data)
        })
        this.socket.on('deleteChat', chatId => {
            app.$store.commit('chats/deleteChat', chatId)
        })
    }
}

const webSockets = new WebSockets

export default webSockets
