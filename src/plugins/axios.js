import axios from 'axios'

const axiosConfig = {
    baseURL: process.env.VUE_APP_API_URL,
    timeout: 5000,
}

const Axios = axios.create(axiosConfig);

export default {
    install: app => {
        app.config.globalProperties.$axios = Axios;
    }
}

export {
    Axios,
    axios
}