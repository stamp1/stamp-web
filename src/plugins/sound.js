class Sound {
    install(app) {
        app.config.globalProperties.$sound = this;
    }
    async newMessage() {
        const audio = new Audio('/sounds/new_message.mp3');
        await audio.play();
    }
}

const sound = new Sound

export default sound
