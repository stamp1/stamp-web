const $electron = window.electron

class Electron {
    install(app) {
        if (this.constructor.isElectron()) {
            app.config.globalProperties.$electron = $electron;
        }
    }
    static isElectron() {
        return Boolean(window && window.process && window.process.type)
    }
}

const electron = new Electron

export default electron
export { $electron, Electron }
