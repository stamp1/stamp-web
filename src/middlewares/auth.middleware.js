import store from '@/store'
import {Axios} from "@/plugins/axios";
import app from "@/main";

let usedAxiosInterceptor = false;

export default {
    beforeEachMiddleware: async (to, from, next) => {
        const search = window.location.hash.slice(2)
        const urlSearchParams = new URLSearchParams(search);
        const params = Object.fromEntries(urlSearchParams.entries());
        if (params.token) {
            store.commit('auth/auth', params.token)
        }

        Axios.defaults.headers.common['Authorization'] = store.state.auth.token;

        if (!usedAxiosInterceptor) {
            usedAxiosInterceptor = true;
            Axios.interceptors.response.use(
                (response) => response,
                (err) => {
                    // if it auth - dont show error
                    console.log(err)
                    if (err?.response?.config?.url === '/auth/login' && err.response.status === 404) {
                        // ignore if user not exist
                    } else {
                        app.$toast.error(
                            err?.response?.data?.message
                                ? err?.response?.data?.message
                                : err.message);
                    }
                    // if status == Not authenticated
                    // or response not founded (for example - server down and dont send any response)
                    // -> logout user
                    if (
                        err?.response?.status === 401 && store.getters['auth/authed']
                        ||
                        !err.response && store.getters['auth/authed']
                    ) {
                        store.dispatch('auth/logout')
                    }
                    return Promise.reject(err);
                })
        }

        if (to.meta.requiresAuth && !store.getters['auth/authed']) {
            next('/auth');
        }
        if (to.meta.requiresUnAuth && store.getters['auth/authed']) {
            next('/')
        }

        // initialization if not initialized and authed
        if (!store.getters['auth/inited'] && store.getters['auth/authed']) {
            store.dispatch('auth/initialize');
        }
        next();
    }
}
