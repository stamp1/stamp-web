import {createRouter, createWebHashHistory} from 'vue-router'

import Welcome from '../views/pages/Welcome'
import Chat from '../views/pages/Chat'
import Auth from '../views/pages/Auth'
import Settings from '../views/pages/Settings'
import NotFound from '../views/pages/NotFound'

import authMiddleware from '@/middlewares/auth.middleware'

const routes = [
  {
    path: '/',
    name: 'Welcome',
    component: Welcome,
  },
  {
    path: '/chat',
    name: 'Chat',
    component: Chat,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/auth',
    name: 'Auth',
    component: Auth,
    meta: {
      requiresUnAuth: true
    }
  },
  {
    path: '/:catchAll(.*)',
    name: '404',
    component: NotFound,
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach(authMiddleware.beforeEachMiddleware)

router.afterEach((to) => {
  document.title = to.meta.title || to.name + ' - Stamp';
})

export default router
