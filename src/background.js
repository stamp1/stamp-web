'use strict'

import {app, protocol, BrowserWindow, ipcMain, Notification, Menu, Tray} from 'electron'
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib'
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer'
import path from 'path'
const isDevelopment = process.env.NODE_ENV !== 'production'
const iconPath = path.join(__dirname, '../public/icon.png')

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: 'app', privileges: { secure: true, standard: true } }
])

let win, isQuiting = false

async function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    minWidth: 330,
    minHeight: 600,
    height: 600,
    center: true,
    icon: iconPath,
    webPreferences: {
      nodeIntegration: true
    },
    autoHideMenuBar: true
  })

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL)
    if (!process.env.IS_TEST) win.webContents.openDevTools()
  } else {
    createProtocol('app')
    // Load the index.html when not in development
    win.loadURL('app://./index.html')
  }

  const appIcon = new Tray(iconPath)

  const contextMenu = Menu.buildFromTemplate([
    {
      label: 'Show App', click: function () {
        win.show()
      }
    },
    {
      label: 'Quit', click: function () {
        isQuiting = true
        app.quit()
      }
    }
  ])

  appIcon.setContextMenu(contextMenu)

  win.on('close', function (event) {
    console.log(isQuiting);
    if(!isQuiting){
      event.preventDefault();
      win.hide();
    }
    return false;
  })
}


// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    // app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.

  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
let tray = null
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      // await installExtension(VUEJS_DEVTOOLS)
    } catch (e) {
      console.error('Vue Devtools failed to install:', e.toString())
    }
  }
  createWindow()
})

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === 'win32') {
    process.on('message', (data) => {
      if (data === 'graceful-exit') {
        // app.quit()
      }
    })
  } else {
    process.on('SIGTERM', () => {
      // app.quit()
    })
  }
}

class Service {
  constructor() {
    ipcMain.on('new_chat_message', (event, data) => {
      const not = new Notification({
        title: data.title,
        body: data.content,
        icon: '',
        sound: false,
      })
      not.show()
      not.on('click', function () {
        win.show()
      })
      event.reply('new_chat_message_clicked', data)
    })
  }
}

new Service()
