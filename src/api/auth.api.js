import {Axios} from "@/plugins/axios";

export default {
    login(login, password) {
        return Axios.post('/auth/login', {
            login: login,
            password: password
        });
    },
    register(login, password) {
        return Axios.post('/auth/register', {
            login: login,
            password: password
        });
    },
    logout() {
        return Axios.post('/auth/logout');
    },
}