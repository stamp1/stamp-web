import {Axios} from "@/plugins/axios";

export default {
    getCurrentUser() {
        return Axios.get(`/user/`).then(res => res.data);
    },
    getFriends() {
        return Axios.get(`/user/friends`).then(res => res.data);
    },
    search(name) {
        return Axios.get(`/user/search/${name}`).then(res => res.data.candidates);
    },
}