import {Axios} from "@/plugins/axios";

export default {
    loadChats() {
        return Axios.get('/chat/');
    },
    getChat(id) {
        return Axios.get(`/chat/${id}/15`).then( res => res.data )
    },
    createChat(recipient) {
        return Axios.put('/chat/', {recipient}).then(res => res.data.chat)
    },
    sendMessage(data) {
        return Axios.put('/chat/message/', data).then(res => res.data.message)
    },
    readMessage(messageId) {
        return Axios.patch(`/chat/message/read/${messageId}`);
    },
    deleteMessage(messageId) {
        return Axios.delete(`/chat/message/${messageId}`);
    },
    saveEdited(messageId, content) {
        return Axios.patch(`/chat/message/${messageId}`, {content}).then(res => res.data);
    },
    deleteChat(chatId) {
        return Axios.delete(`/chat/${chatId}`);
    }
}